## Hummingbird for training
The folder Hummingbird contains full Unity3D project set up for training. 
It has eight "game worlds" running simultaneously. You can tweek the training_config.yaml
file found in the folder Configuration. When trainig you use the scene called Training.
## Hummingbird running with trained AI agent
If you choose the scene called FlowerIsland, you can watch two AI hummingbirds compete against each other. 
If you want to try playing, change the settings of player (when you are in scene-mode!)
go to Player, Behaviour Parameter, Behaviour Type and change from default  to using "Heuristics".
Then you are good to go!
## Technical
Based on Unity version 2019.4.10f1
## The Hummingbird Tutorial
You find the complete tutorial here: https://learn.unity.com/course/ml-agents-hummingbirds
## Notifications
The training_config.yaml in the tutorial is not working becase of a change in configuration
policies of ML-agents. The file in the folder Configuration is therefore modified, and
at least currently working.
