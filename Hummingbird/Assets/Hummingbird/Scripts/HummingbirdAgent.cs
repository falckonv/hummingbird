
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine;

/// <summary>
/// A hummingbird Machine Learning Agent
/// </summary>


	public class HummingbirdAgent : Agent
	{
	    
	[Tooltip("Force to apply when moving")]
	public float moveForce = 2f;

	[Tooltip("Speed to pitch up or down")]
	public float pitchSpeed = 100f;

	[Tooltip("Speed to rotate around the up axis")]
	public float yawSpeed = 100f;

	[Tooltip("Transform at the tip of the beak")]
	public Transform beakTip;

	[Tooltip("The agents camera")] 
	// Not used by neural network only human player
	public Camera agentCamera;

	[Tooltip("Whether this is training mode or gameplay")]
	public bool trainingMode;


	// The rigidbody of the agent
	new private Rigidbody rigidbody;  // Unity used to quicly look up the rigid body 

	// The flower area that the agent is in
	private FlowerArea flowerArea;

	// The nearest flower to the agent
	private Flower nearestFlower;

	// Allows for smoother pitch change (necessary for neuralnetwork stearing smooth)
	private float smoothPitchChange = 0f;
	private float smoothYawChange = 0f;

	// Maximum angle that the bird can pitch up or down
	private const float MaxPitchAngle = 80f;  // Not max 90 degrees!


	// Maximum distance from the beak tip to accept nectar collision
	private const float BeakTipRadius = 0.008f; // Only accept hit of nectar in this radius

	// Wheter the agent is frozen (intentionally not flying)
	private bool frozen = false; // Still observing, but no actions taken

	/// <summary>
	/// The amount of nectar the agent has obtained in this episode
	/// </summary>
	public float NectarObtained { get; private set; }


	// FUNCTIONS _ OVERRIDE

	public override void Initialize() {
	//base.Initialize(); // If it is something in the overrided class initialise (IS NOT)
	rigidbody = GetComponent<Rigidbody>();
	flowerArea = GetComponentInParent<FlowerArea>();

	// If not in training mode, no max step - play forever
	if (!trainingMode) MaxStep = 0;
	}

	/// <summary>
	/// Reset the agent when an episode begins
	/// </summary>

	public override void OnEpisodeBegin() {
	if (trainingMode) {
	// Only reset flowers in trainig when there is one agent per area
	flowerArea.ResetFlowers();
	}

	NectarObtained = 0f;

	// Stop movement
	rigidbody.velocity = Vector3.zero;
	rigidbody.angularVelocity = Vector3.zero;

    // Default to spawning in front of a flower
	bool inFrontOfFlower = true;

	if (trainingMode) 
	{
    // Spawn in front of flower 50% of the time
	inFrontOfFlower = UnityEngine.Random.value > .5f;
	}

	// Move agent to a new random position (not in a bush or colliding)
	MoveToSafeRandomPosition(inFrontOfFlower);
	// Recalculate the nearest flower now that the agent has moved
	UpdateNearestFlower();
	}


	/// <summary>
	/// Called when an action is received from either the player input of the neural network
	/// vectorAction[i] represents:
	/// Index 0: move vector x (+1 = right, -1 = left)
	/// Index 1: move vector y (+1 = up, -1 = down)
	/// Index 2: move vector z (+1 = forward, -1 = backward)
	/// Index 3: pitch angle (+1 = pitch up, -1 = pitch down)
	/// Index 4: yaw angle (+1 = turn right, -1 = turn left)
	/// </summary>
	/// <param name="vectorAction">The actions to take</param>
	public override void OnActionReceived(float[] vectorAction) {
	// Dont take action if frozewn
	if (frozen) return;  // This will not happen under training, but can happen in gamemode

	// Calculate movement vector
	Vector3 move = new Vector3(vectorAction[0], vectorAction[1], vectorAction[2]);

	// Add force in the direction of the move vector
	rigidbody.AddForce(move * moveForce);

	// Handle rotation (rotate ML-Agent, does not use force, only rotate - make jitter)
	// Make rotation smooth 
	Vector3 rotationVector = transform.rotation.eulerAngles;

	// Calculate ptich and yaw from input and then rotation
	float pitchChange = vectorAction[3];
	float yawChange = vectorAction[4];
	// Smoothing (pitchChange = target):
	smoothPitchChange = Mathf.MoveTowards(smoothPitchChange, pitchChange, 2f * Time.fixedDeltaTime);
	smoothYawChange = Mathf.MoveTowards(smoothYawChange, yawChange, 2f * Time.fixedDeltaTime);

	// Calculate new pitch and yaw based on smoothed values
	// Clamp pitch to avoid flipping upside down
	float pitch = rotationVector.x + smoothPitchChange * Time.fixedDeltaTime * pitchSpeed;
	if (pitch > 180f) {
		pitch -= 360f;
	}
	pitch = Mathf.Clamp(pitch, -MaxPitchAngle, MaxPitchAngle);

	float yaw = rotationVector.y + smoothYawChange * Time.fixedDeltaTime * yawSpeed;

	// Apply new rotation
	transform.rotation = Quaternion.Euler(pitch, yaw, 0f);
	}

	/// <summary>
	/// Collect vector observation from the environment
	/// </summary>
	/// <param name="sensor">The vector sensor </param>
	public override void CollectObservations(VectorSensor sensor) {

    	// If nearestFlower is null, observ an empty array and return early
	// Get zeros until sensors are set
	if (nearestFlower == null) {
		sensor.AddObservation(new float[10]);
		return;
	}
	// (4 observations) Observe the agents local rotation (in the local space, not word space)
	// Give the bird some sense of it pointing up/down left/right, but not where it 
	// is in the world space 
	sensor.AddObservation(transform.localRotation.normalized);

	// Get a vector from the beak tip to the nearest flower
	Vector3 toFlower = nearestFlower.FlowerCenterPosition - beakTip.position;

	// (3 observations) Observe a normalized vector pointing to the nearest flower 
	sensor.AddObservation(toFlower.normalized);

	// (1 observation)Observe a dot product that says if the beak tip is in front of the flower
	// (+1 means that the beak tip is directly in front of the flower, -1 means directly behind)
	// -nearestFlower.FlowerUpVector = the DOWN vector 
	sensor.AddObservation(Vector3.Dot(toFlower.normalized, -nearestFlower.FlowerUpVector.normalized));
    
	// (1 observation) Observe a dot product that says if the beak tip pointing toward the flower
	// (+1 means that the beak tip is directly in front of the flower, -1 means directly behind)
	// -nearestFlower.FlowerUpVector = the DOWN vector 
	sensor.AddObservation(Vector3.Dot(beakTip.forward.normalized, -nearestFlower.FlowerUpVector.normalized));

    // (1 observation) Observe the relative distance from the beak tip to the flower 
	sensor.AddObservation(toFlower.magnitude / FlowerArea.AreaDiameter);

	// 10 total observation (need to know when hooking up the hummingbird in UNITY)

	}

	/// <summary>
	/// When Behavior Type is set to "Heuristic Only" on the agents Behavior Parameters,
	/// this function will be called. Its return values will be fed into
	/// <see cref="OnActionRecieived(float[]"/> instead of using the neural network.
	/// This is used for user keyboard or other inputs
	/// </summary>
	/// <param name="actionsOut"> An output action array </param>
	public override void Heuristic(float[] actionsOut) {
	// Placeholders for all moving and turnings
	Vector3 forward = Vector3.zero;  // Default to not moving
	Vector3 left = Vector3.zero;
	Vector3 up = Vector3.zero;
	float pitch = 0f;
	float yaw = 0f;

	// Convert keyboard inputs to movement and turning all values between -1 and +1

	// Forward/backward
	if (Input.GetKey(KeyCode.W)) forward = transform.forward;
	else if (Input.GetKey(KeyCode.S)) forward = -transform.forward;

	// Left/right
	if (Input.GetKey(KeyCode.A)) left = -transform.right;
	else if (Input.GetKey(KeyCode.D)) left = transform.right;

    	// Forward/backward
	if (Input.GetKey(KeyCode.E)) up = transform.up;
	else if (Input.GetKey(KeyCode.C)) up = -transform.up;

	// Pitch up/down
	if (Input.GetKey(KeyCode.UpArrow)) pitch = 1f;
	else if (Input.GetKey(KeyCode.DownArrow)) pitch = -1f;

	// Turn left/right
	if (Input.GetKey(KeyCode.LeftArrow)) yaw = -1f;
	else if (Input.GetKey(KeyCode.RightArrow)) yaw = 1f;

	// Combine the movement vectors and normalize
	Vector3 combined = (forward + left + up).normalized;

	// Add the 3 movment values, pitch, and yaw to the actionsOut array
	actionsOut[0] = combined.x;
	actionsOut[1] = combined.y;
	actionsOut[2] = combined.z;
	actionsOut[3] = pitch;
	actionsOut[4] = yaw;

	}

	/// <summary>
	/// Prevent the agent from moving and taking actions
	/// </summary>
	public void FreezeAgent() {
		Debug.Assert(trainingMode == false, "Freeze/Unfreeze not supported in training");
		frozen = true;
		rigidbody.Sleep();
	}

	/// <summary>
	/// Resume agent movment and actions
	/// </summary>
	public void UnfreezeAgent() {
		Debug.Assert(trainingMode == false, "Freeze/Unfreeze not supported in training");
		frozen = false;
		rigidbody.WakeUp();
	}


	/// <summary>
	/// Move agent to safe random position. Does not collide with anything
	/// If in front of flower, also point beak at the flower
	/// </summary>
	/// <param name="inFrontOfFlower"> Wheter to choose a spot in front of a flower </param>
	private void MoveToSafeRandomPosition(bool inFrontOfFlower) {
	bool safePositionFound = false;
	int attemptsRemaining = 100; // Prevent an infinite loop
	Vector3 potentialPosition = Vector3.zero;
	Quaternion potentialRotation = new Quaternion();

	// Loop util a safe position is found or we run out of attempts
	while (!safePositionFound && (attemptsRemaining > 0)) {
	attemptsRemaining--;
	if (inFrontOfFlower) {

	// Pick a random flower
	Flower randomFlower = flowerArea.Flowers[UnityEngine.Random.Range(0, (flowerArea.Flowers.Count))];
    
	// Position 10 to 20 cm in front of the flower
	float distanceFromFlower = UnityEngine.Random.Range(.1f, .2f);
	potentialPosition = randomFlower.transform.position + randomFlower.FlowerUpVector * distanceFromFlower;

	Vector3 toFlower = randomFlower.FlowerCenterPosition - potentialPosition;
	potentialRotation = Quaternion.LookRotation(toFlower, Vector3.up);
	}

	else {

	// Pick a random height from the ground	
	float height = UnityEngine.Random.Range(1.2f, 2.5f);

    // Pick a random radius from the center of the area
	float radius = UnityEngine.Random.Range(2f, 7f);

    // Pick a random direction rotated around the y axis
	Quaternion direction = Quaternion.Euler(0f, UnityEngine.Random.Range(-180f, 180f), 0f);

    // Combine height, radius, and direction to pick a potential position
	potentialPosition = flowerArea.transform.position + Vector3.up * height + direction * Vector3.forward * radius;

    // Choose and set random starting pitch and yaw
  	float pitch = UnityEngine.Random.Range(-60f, 60f);
	float yaw = UnityEngine.Random.Range(-180f, 180f);
	potentialRotation = Quaternion.Euler(pitch, yaw, 0f);
	}
	
	// Check to see if the agent will collide with anything
	Collider[] colliders = Physics.OverlapSphere(potentialPosition, 0.05f);
	
	// Safe position has been found if no colliders are overlapped
	safePositionFound = colliders.Length == 0;
	}
	Debug.Assert(safePositionFound, "Could not find a safe position to spawn");

	transform.position = potentialPosition;
	transform.rotation = potentialRotation;

	}

	/// <summary>
	/// Update the nearest flower to the agent
	/// </summary>
	private void UpdateNearestFlower()
	{
	    foreach (Flower flower in flowerArea.Flowers)
		{
		if (nearestFlower == null && flower.HasNectar)
		{
		// No current nearest flower and this flower has nectar - so set to this flower
		nearestFlower = flower;
		}
		else if (flower.HasNectar)
		{
			// Calculate distance to this flower and distance to the current nearests flower
			float distanceToFlower = Vector3.Distance(flower.transform.position, beakTip.position);
			float distanceToCurrentNearestFlower = Vector3.Distance(nearestFlower.transform.position, beakTip.position);
			// If current nearest flower is empty OR this flower is closer, update the nearest flower
			if (!nearestFlower.HasNectar || distanceToFlower < distanceToCurrentNearestFlower)
			{
			nearestFlower = flower;
			}
		}
	}
}

	/// <summary>
	/// Called when the agents collider enters a trigger collider
	/// </summary>
	/// <param name="other">The trigger collider</param>
	private void OnTriggerEnter(Collider other)
	{
		TriggerEnterOrStay(other);
	}


	/// <summary>
	/// Called when the agents collider enters a trigger collider
	/// </summary>
	/// <param name="other"> The trigger collider </param>
	private void OnTriggerStay(Collider other) {
		TriggerEnterOrStay(other);
	}

	/// <summary>
	///Handles when the agents collider enters or stays in a trigger collider
	/// </summary>
	/// <param name="collider"> The trigger collider </param>
	private void TriggerEnterOrStay(Collider collider)
	{

	// Chek if agent is colliding with nectar
	if (collider.CompareTag("nectar")) {
		Vector3 closestPointToBeakTip = collider.ClosestPoint(beakTip.position);

		// Check if the closest collision point is close to the beak tip
		// Note: a collision wiht anything but the beak tip should not count
		// Only reward when in the beak
        if (Vector3.Distance(beakTip.position, closestPointToBeakTip) < BeakTipRadius)
		{
			Flower flower = flowerArea.GetFlowerFromNectar(collider);

			// Attempt to take .01 nectar (per fixed timestep meaning every .02 sec or 50 times/sec)
			float nectarReceived = flower.Feed(.01f);

			// Keep track of nectar obtained
			NectarObtained += nectarReceived;

			if (trainingMode)
			{
				// Calculate rewards
				// Giving bonus if beak is pointing directly at the flower 
				float bonus = .02f * Mathf.Clamp01(Vector3.Dot(transform.forward.normalized, -nearestFlower.FlowerUpVector.normalized));
				AddReward(.01f + bonus);
			}

            // If flower is empty, udate the nearest flower
			if(!flower.HasNectar) {
				UpdateNearestFlower();
			}
		}

	}

}


	/// <summary>
	///Called when the agent collides with something solid
	/// </summary>
	/// <param name="collision">The collision info</param>
	private void OnCollisionEnter(Collision collision)
	{
		if (trainingMode && collision.collider.CompareTag("boundary"))
		{
			// Collides with the area boundary - give a negative reward
			// Not go too negative!
			AddReward(-.5f);
		}
	}

	/// <summary>
	/// Called every frame
	/// </summary>
	private void Update()
	{
		// Draw a line form the beak tip to the nearest flower
		if (nearestFlower != null)
		{
			Debug.DrawLine(beakTip.position, nearestFlower.FlowerCenterPosition, Color.green);
		}
	}

	/// <summary>
	/// Called every .02 seconds
	/// </summary>
	private void FixedUpdate()
	{
		// Handles the situation if an oponent is stealing the nectar in nearby flowers
		if (nearestFlower != null && !nearestFlower.HasNectar)
		{
		UpdateNearestFlower();
	}
}

}


