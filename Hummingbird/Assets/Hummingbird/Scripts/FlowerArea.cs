using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages a collection of flower plants and attached flowers
/// </summary>

public class FlowerArea : MonoBehaviour
{
        // Diameter of the area where the agent and flower can be
        // based on the world size (20 meter is max distance)
public const float AreaDiameter = 20f;

// List of all flower plants in this flower area (flower plants have mulitple flowers)
private List<GameObject> flowerPlants;
// A lookup dictionary for looking up a flower from a nectar collider
private Dictionary<Collider, Flower> nectarFlowerDictionary; 
    /// <summary>
    /// The list of all flowers in the flower area
    /// </summary>
public List<Flower> Flowers { get; private set; }
  /// <summary>
    /// Reset the flowers and flower plants
    /// </summary>
public void ResetFlowers()
{
	// Rotate all flower plat around the Y axis and subtly aroud X and Z
 	foreach (GameObject flowerPlant in flowerPlants)
	{
	float xRotation = UnityEngine.Random.Range(-5f,5f); // random rotation around x-axis
	float yRotation = UnityEngine.Random.Range(-180f,180f); // random rotation around x-axis
	float zRotation = UnityEngine.Random.Range(-5f,5f); // random rotation around x-axis
	flowerPlant.transform.localRotation = Quaternion.Euler(xRotation, yRotation, zRotation);
	}

	// Reset all flowers
	foreach (Flower flower in Flowers)
	{
	flower.ResetFlower();
	}
}

/// <summary>
/// Gets the <see cref="Flower"/> that a nectar collider belongs to
/// </summary>
/// <param name="collider">The nectar collider</param>
/// <returns>The matching flower</returns>
public Flower GetFlowerFromNectar(Collider collider)
{
	return nectarFlowerDictionary[collider];
}
/// <summary>
/// Called when the area wakes up
/// </summary>
private void Awake()
{
	// Initialize variables
	flowerPlants = new List<GameObject>();
	nectarFlowerDictionary = new Dictionary<Collider, Flower>();
	Flowers = new List<Flower>();
 	
	// Find all flowers that are children of this GameObject/Transform
	FindChildFlowers(transform);
}

/// <summary>
/// Recursively finds all flowers and flower plants that are children of a parent transform
/// </summary>
/// <param name="parent">The parent of the children to check</param>
private void FindChildFlowers(Transform parent)
{
	for (int i = 0; i < parent.childCount; i++) 
	{
	Transform child = parent.GetChild(i);

	if (child.CompareTag("flower_plant"))
	{
	// Found a flower plant, add it to the flowerPlants list
	flowerPlants.Add(child.gameObject);

	// Look for flowers within the flower plant (should be)
	FindChildFlowers(child);
	}
	else
	{
	// Not a flower plant, look for a Flower component
	Flower flower = child.GetComponent<Flower>();
		if (flower != null)
		{
		// Found a flower, add it to the Flowers list
		Flowers.Add(flower);
		// Add the nectar collider to the lookup dictionary
		nectarFlowerDictionary.Add(flower.nectarCollider, flower);
		// Note: there are no flowers that are children of other flowers
		}
		else
        {
        // Flower component not found, so check children
        FindChildFlowers(child);
        }
		}
	}
	}	
}  

